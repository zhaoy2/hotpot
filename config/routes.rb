Rails.application.routes.draw do
  resources :languages

  resources :translations

  resources :orders, :only => [:index, :show] do
    get :pay, :on => :member
    get :pay_later, :on => :member
    get :process_payment, :on => :member
    get :cart, :on => :member
    match :setting, :on => :collection, :via => [:get, :patch]
  end

  namespace :xapi do
    resources :orders, :only => [:index, :update]
  end

  get 'cart', :to => 'cart#index', :as => 'cart_index'
  get 'cart/update'
  get 'cart/checkout'
  post 'cart/checkout', :to => 'cart#convert', :as => 'cart_convert'
  get 'cart/:sku', :to => 'cart#add', :as => 'cart_addsku'

  root 'home#index'
  get '/list/:uri', :to => 'home#list', :as => 'home_list'
  get '/show/:uri/:sku', :to => 'home#show', :as => 'home_show'

  devise_for :users
end
