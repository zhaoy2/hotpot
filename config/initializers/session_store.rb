# Be sure to restart your server when you modify this file.

#Rails.application.config.session_store :cookie_store, key: '_hotpot_session'

Rails.application.config.session_store :redis_session_store, {
  key: 'session',
  redis: {
    expire_after: 30.days,
    key_prefix: 'session:'
  }
}
