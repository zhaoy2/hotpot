class OrdersController < ApplicationController
  respond_to :html
  before_action :authenticate_user!
  before_filter :find_order, :only => [:show, :pay, :pay_later, :process_payment, :cart]
  include PayPal::SDK
  def index
    respond_with @orders = current_user.orders.includes(:order_items => {:dish => :category}).paginate(:page => params[:page], :per_page => 15).order('id DESC')
  end

  def setting
    if params[:user]
      u = User.find(current_user)
      u.update_attributes user_params
      redirect_to setting_orders_path
    end
  end
  def user_params
    params.require(:user).permit([:mobile, :address, :wechat, :birthday])
  end

  def show
    if @order.nil?
      redirect_to :action => :index
      return
    end
    respond_with @order
  end

  def pay
    if @order.paid?
      redirect_to order_path(@order)
      return
    end
    if @order.balance < 0.01
      @order.paid!
      redirect_to order_path(@order)
      return
    end
    if @order.nil?
      redirect_to :action => :index
      return
    end
    @payment = REST::Payment.new({
      :intent => "sale",
      :payer => {
        :payment_method => "paypal" },
      :redirect_urls => {
        :return_url => process_payment_order_url(@order),
        :cancel_url => process_payment_order_url(@order) },
      :transactions => [ {
        :amount => {
          :total => "%.2f" % @order.balance,
          :currency => "USD" },
        :description => "Online Menu Order" } ] } )

    if @payment.create
      @redirect_url = @payment.links.find{|v| v.method == "REDIRECT" }.href
      @order.paypal_payment_id = @payment.id
      @order.save
      @order.payments.create :payment_id => @payment.id, :amount => @order.balance
      redirect_to @redirect_url
    else
      redirect_to order_path(@order)
    end
  end

  def pay_later
    @order.paid!
    redirect_to order_path(@order)
  end

  def process_payment
    # http://10.0.1.40/orders/1/process_payment?token=EC-7040590647086154P&PayerID=2E8ZE9J9VR6T8
    payment = REST::Payment.find(@order.paypal_payment_id)

    if payment.execute(:payer_id => params[:PayerID])
      flash[:success] = "Paid"
      payment_total = payment.transactions.first.amount.total
      @order.balance -= payment_total.to_f
      @order.paid! if @order.balance < 0.01
      p = @order.payments.find_by_payment_id(@order.paypal_payment_id)
      if p
        p.payer_id = params[:PayerID]
        p.paid!
        p.save
      end
      @order.save
      # Success Message
      # Note that you'll need to `Payment.find` the payment again to access user info like shipping address
    else
      flash[:error] = payment.error # Error Hash
    end
    redirect_to order_path(@order)
  end

  def cart
    @cart = Cart.new session
    @order.order_items.each do |i|
      i.count.times { |n| @cart.add i.dish }
    end
    @cart.save!
    redirect_to cart_index_path
  end

  private
  def find_order
    @order = current_user.orders.includes(:order_items => {:dish => [:category]}).find_by_id(params[:id])
  end
end
