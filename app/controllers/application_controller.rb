class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :set_i18n

  def set_i18n
    Translation.lang = session[:lang] || detect_locale
    I18n.locale = Translation.lang == :Chinese ? :cn : :en
  end

  def detect_locale
    return :Chinese if request.env['HTTP_ACCEPT_LANGUAGE'].include? 'zh' rescue :English
    :English
  end
end
