class Xapi::OrdersController < ApplicationController
  respond_to :json
  def index
    @orders = Order.valid.includes([{:order_items => [:dish]}, :payments]).order(:updated_at)
    respond_with @orders
  end
end