class HomeController < ApplicationController
  respond_to :html
  def index
    respond_with @categories = Category.order(:position => :asc).includes(:dishes)
  end

  def list
    uri = params[:uri]
    @category = Category.find_by_uri(uri)
    if @category.nil?
      redirect_to root_path
      return
    end
    respond_with @category
  end

  def show
    sku = params[:sku]
    @dish = Dish.find_by_sku(sku)
    if @dish.nil?
      redirect_to root_path
      return
    end
    respond_with @dish
  end
end
