class CartController < ApplicationController
  before_filter :load_cart
  before_filter :load_sku, :only => [:add, :update]
  before_action :authenticate_user!, :only => [:checkout, :convert]
  def index
    @dishes = Dish.includes(:category)
  end

  def add
    if @item
      @cart.add @item
      @cart.save!
    end
    if params[:js]
      render :plain => 'OK'
    else
      redirect_to :action => :index
    end
  end

  def update
    action = params[:do] || nil
    if @item
      case action
      when 'add', 'remove', 'delete'
        @cart.send action, @item
        @cart.save!
      end
    end
    redirect_to :action => :index
  end

  def checkout
    redirect_to :action => :index if @cart.total < 0.01
    @order = Order.new :address => current_user.address
  end

  def convert
    @order = @cart.to_order order_params
    @order.user = current_user
    @order.save
    redirect_to order_path(@order)
  end

  private
  def load_cart
    @cart = Cart.new session
  end
  def load_sku
    sku = params[:sku]
    @item = Dish.find_by_sku sku
  end
  def order_params
    params.require(:order).permit([:delivery, :address, :shipping])
  end
end
