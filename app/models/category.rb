class Category < ActiveRecord::Base
  has_many :dishes, -> { order(:position => :asc) }
  acts_as_list

  def local_name
    return chinese if Translation.chinese?
    return english if Translation.english?
    Translation.t(english)
  end
end
