class Payment < ActiveRecord::Base
  belongs_to :order
  enum :status => [:unpaid, :paid]
end
