class Dish < ActiveRecord::Base
  belongs_to :category
  acts_as_list :scope => :category

  def local_name
    return chinese if Translation.chinese?
    return english if Translation.english?
    Translation.t(sku)
  end
end
