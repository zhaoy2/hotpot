class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  # :recoverable, :trackable, :validatable, :confirmable
  devise :database_authenticatable, :registerable, :rememberable
  has_many :orders
end
