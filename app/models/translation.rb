class Translation < ActiveRecord::Base
  belongs_to :language
  class << self
    attr_accessor :lang
  end
  def self.t(text, options = nil)
    english = Language.find_by_name(:English)
    lang = Language.find_by_name(@lang) || english
    translation = lang.translations.find_by_key(text)
    translation ||= english.translations.find_by_key(text)
    translation %= options if options
    translation
  end
  def self.chinese?
    return @lang == :Chinese
  end
  def self.english?
    return @lang == :English
  end
end
