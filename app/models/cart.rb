require 'dish'

class Cart
  attr_reader :item_list
  def initialize session
    @session = session
    @item_list = Hash.new
    if session[:cart]
      @item_list = YAML.load(session[:cart]) rescue Hash.new
    end
  end
  def add item
    ci = CartItem.new item
    if @item_list[ci.key]
      @item_list[ci.key].inc
    else
      @item_list[ci.key] = ci
    end
  end
  def remove item
    ci = CartItem.new item
    return if !@item_list[ci.key]
    @item_list[ci.key].dec
    @item_list.delete ci.key if @item_list[ci.key].empty?
  end
  def delete item
    ci = CartItem.new item
    @item_list.delete ci.key
  end
  def count
    @item_list.values.sum(&:count)
  end
  def total
    @item_list.values.sum(&:total)
  end
  def save!
    @session[:cart] = YAML.dump(@item_list)
  end
  def to_order params
    order = Order.new params
    order.shipping = params['shipping'] == 0 ? 0 : 2
    order.shipping = 0 if order.carry_out?
    order.total = total + order.shipping
    order.balance = order.total
    @item_list.each do |key, item|
      o = order.order_items.new
      o.dish_id = item.id
      o.count = item.count
      o.total = item.total
    end
    @session[:cart] = nil
    @item_list = []
    order
  end
end

class CartItem
  attr_reader :name, :count, :key, :uri, :price, :id
  def initialize item
    @id = item.id
    @name = item.local_name
    @uri = item.category.uri
    @price = item.price
    @count = 1
    @key = item.sku
  end
  def inc
    @count += 1
  end
  def dec
    @count -= 1
  end
  def empty?
    @count <= 0
  end
  def total
    @price * @count
  end
  def to_s
    s = []
    s << @name
    s << "x#{@count}" if @count > 0
    s.join ''
  end
end
