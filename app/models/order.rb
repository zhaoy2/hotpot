class Order < ActiveRecord::Base
  has_many :order_items
  belongs_to :user
  has_many :payments
  enum :delivery => [:carry_out, :delivery]
  enum :status => [:created, :paid, :printed]
  scope :valid, -> { where('status BETWEEN 1 AND 3') }
  def valid_payments
    payments.where('status > 0')
  end

  def expired?
    created? && created_at < 6.hours.ago
  end

  def self.delivery_options
    deliveries.keys.map{|k| [k, I18n.t(k)]}
  end

  def self.shipping_options
    [[0, I18n.t(:on_campus_free)], [1, I18n.t(:in_town)]]
  end

  def as_json(options = {})
    super :include => {
        :order_items => {
          :only => [:id, :count, :total],
          :include => {
            :dish => {
              :only => [:chinese, :english, :sku, :price]
            }
          }
        },
        :valid_payments => {}
      }
  end
end
