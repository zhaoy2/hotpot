module OrderHelper
  def time_to_s time
    time.strftime '%m/%d/%y %H:%M'
  end
  def show_label order
    if order.paid?
      if order.balance > 0.01
        return content_tag 'span', 'Pay on delivery', :class => 'label label-normal'
      else
        return content_tag 'span', 'Paid', :class => 'label label-success'
      end
    end
    return content_tag 'span', 'Order Received', :class => 'label label-normal' if order.printed?
    return content_tag 'span', 'Expired', :class => 'label label-default' if order.expired?
    
    (link_to t('order.pay_later'), pay_later_order_path(order), :class => 'btn btn-primary btn-xs') + ' ' + (link_to fa_icon('cc-paypal 2x', :class => 'text-primary'), pay_order_path(order))
  end
end
