module DishHelper
  def render_title item
    dish = item if item.is_a? Dish
    dish ||= cache_dishes[item.id] if item.respond_to? :id
    return "Internal error" if dish.nil?
    render_item dish
  end

  def render_item dish
    s = StringIO.new
    s << content_tag(:div, dish.local_name, :class => 'tooltip-item-name')
    s << content_tag(:div, dish.category.local_name, :class => 'tooltip-category')
    s << content_tag(:div, dish.description, :class => 'tooltip-description')
    s << content_tag(:div, "$%.2f" % dish.price, :class => 'tooltip-price')
    s.string
  end

  def cache_dishes
    @cache_dishes ||= Dish.includes(:category).inject({}){|h,a| h[a.id.to_i] = a; h}
  end
end
