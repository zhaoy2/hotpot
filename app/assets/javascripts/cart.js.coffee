$ ->
  $('.order_shipping').addClass('hidden')
  $('.order_address').addClass('hidden')
  $('#order_shipping_0').click()
  $('.dish').tooltip
    container: 'body'
    html: true
    template: '<div class="tooltip" role="tooltip"><div class="tooltip-inner"></div></div>'
  $('#order_delivery_carry_out').click ->
    $('.order_shipping').addClass('hidden')
    $('.order_address').addClass('hidden')
  $('#order_delivery_delivery').click ->
    $('.order_shipping').removeClass('hidden')
    $('.order_address').removeClass('hidden')
