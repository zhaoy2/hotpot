# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140929105114) do

  create_table "categories", force: true do |t|
    t.string   "chinese",    default: "", null: false
    t.string   "english",    default: "", null: false
    t.integer  "position",                null: false
    t.string   "uri",                     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "categories", ["position"], name: "index_categories_on_position", using: :btree
  add_index "categories", ["uri"], name: "index_categories_on_uri", unique: true, using: :btree

  create_table "dishes", force: true do |t|
    t.integer  "category_id",                  null: false
    t.string   "chinese",     default: "",     null: false
    t.string   "english",     default: "",     null: false
    t.string   "sku",                          null: false
    t.text     "description",                  null: false
    t.integer  "position",                     null: false
    t.string   "image"
    t.float    "price",       default: 1001.0, null: false
    t.integer  "limit"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "invoices", force: true do |t|
    t.integer  "user_id",    null: false
    t.integer  "order_id",   null: false
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "languages", force: true do |t|
    t.string   "name",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "order_items", force: true do |t|
    t.integer "order_id", null: false
    t.integer "dish_id",  null: false
    t.integer "count"
    t.float   "total"
  end

  create_table "orders", force: true do |t|
    t.integer  "user_id",                         null: false
    t.float    "total",                           null: false
    t.integer  "delivery",          default: 0,   null: false
    t.float    "shipping",          default: 0.0, null: false
    t.string   "address"
    t.string   "paypal_payment_id"
    t.float    "balance",                         null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "status",            default: 0,   null: false
    t.integer  "waiting"
  end

  create_table "payments", force: true do |t|
    t.integer  "order_id",               null: false
    t.string   "payment_id",             null: false
    t.string   "payer_id"
    t.integer  "status",     default: 0, null: false
    t.float    "amount",                 null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "payments", ["order_id"], name: "index_payments_on_order_id", using: :btree

  create_table "translations", force: true do |t|
    t.integer  "language_id", null: false
    t.string   "key",         null: false
    t.text     "text",        null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "translations", ["language_id"], name: "index_translations_on_language_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "mobile",              limit: 20
    t.string   "address",             limit: 200
    t.string   "wechat",              limit: 20
    t.string   "birthday",            limit: 5
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email",                           default: "", null: false
    t.string   "encrypted_password",              default: "", null: false
    t.datetime "remember_created_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree

end
