class CreateTranslations < ActiveRecord::Migration
  def change
    create_table :translations do |t|
      t.references :language, :index => true, :null => false
      t.string :key, :index => true, :null => false
      t.text :text, :null => false

      t.timestamps
    end
  end
end
