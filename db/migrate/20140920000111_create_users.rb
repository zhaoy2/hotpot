class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :mobile, :limit => 20
      t.string :address, :limit => 200
      t.string :wechat, :limit => 20
      t.string :birthday, :limit => 5

      t.timestamps
    end
  end
end
