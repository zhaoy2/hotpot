class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.references :user, :null => false
      t.references :order, :null => false
      t.text :content

      t.timestamps
    end
  end
end
