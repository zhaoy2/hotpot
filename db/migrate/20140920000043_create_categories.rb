class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :chinese, :null => false, :default => ''
      t.string :english, :null => false, :default => ''
      t.integer :position, :null => false
      t.string :uri, :null => false

      t.index :uri, :unique => true
      t.index :position

      t.timestamps
    end
  end
end
