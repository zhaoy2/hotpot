class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.references :user_id, :null => false, :index => true
      t.float :total, :null => false
      t.integer :delivery, :null => false, :default => 0
      t.float :shipping, :null => false, :default => 0
      t.string :address
      t.string :paypal_payment_id
      t.float :balance, :null => false
      t.integer :status, :default => 0, :null => false
      t.integer :waiting
      t.timestamps
    end
  end
end
