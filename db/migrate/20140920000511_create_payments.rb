class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.references :order, :index => true, :null => false
      t.string :payment_id, :index => true, :null => false
      t.string :payer_id
      t.integer :status, :default => 0, :null => false
      t.float :amount, :null => false

      t.timestamps
    end
  end
end
