class CreateOrderItems < ActiveRecord::Migration
  def change
    create_table :order_items do |t|
      t.references :order, :null => false
      t.references :dish, :null => false
      t.integer :count
      t.float :total
    end
  end
end
