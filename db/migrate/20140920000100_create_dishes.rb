class CreateDishes < ActiveRecord::Migration
  def change
    create_table :dishes do |t|
      t.references :category, :null => false
      t.string :chinese, :null => false, :default => ''
      t.string :english, :null => false, :default => ''
      t.string :sku, :null => false
      t.text :description, :null => false
      t.integer :position, :null => false
      t.string :image
      t.float :price, :null => false, :default => 1001
      t.integer :limit

      t.timestamps
    end
  end
end
